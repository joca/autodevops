package object;

public class MyObject {

	private double somme;

	public MyObject(double a, double b) {
		this.somme = a + b;
		System.out.println("la valeur actuelle est : " + this.somme);
	};
	
	public double add(double a) {
		this.somme = this.somme + a;
		System.out.println("l'addition donne : " + this.somme);
		return this.somme;	
	}
	
	public double substract(double a) {
		this.somme = this.somme - a;
		System.out.println("la soustraction donne : " + this.somme);
		return this.somme;		
	}
	
	public void print() {
		System.out.println("la valeur actuelle est : " +this.getResult());
	}

	public double getResult() {
		return somme;
	}
}
